# Generated by Django 4.1 on 2022-09-04 10:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('diary', '0003_alter_diary_options_alter_note_options_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='diary',
            old_name='diary_type',
            new_name='kind',
        ),
    ]
