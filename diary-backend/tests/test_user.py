import pytest
from rest_framework import status


@pytest.mark.django_db
def test_signup_user(client):
    payload = dict(
        email='mark@email.com',
        password='mark@email.com',
        first_name='Mark',
        last_name='Johnson'
    )

    headers = {'content_type': 'application/json'}
    response = client.post('/api/v1/accounts/signup/', payload, headers=headers)
    data = response.data

    assert data['email'] == payload['email']
    assert 'password' not in data
    assert data['first_name'] == payload['first_name']
    assert data['last_name'] == payload['last_name']


@pytest.mark.django_db
def test_login_user(user, client):
    # payload = dict(
    #     email='mark@email.com',
    #     password='mark@email.com',
    #     first_name='Mark',
    #     last_name='Johnson'
    # )

    # client.post('/api/v1/accounts/signup/', payload)
    response = client.post('/api/v1/accounts/login/', dict(email='mark@email.com', password='mark@email.com'))
    assert 'token' in response.data
    token = response.data['token']
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_login_user_fail(client):
    response = client.post('/api/v1/accounts/login/', dict(email='sara@email.com', password='mark@email.com'))

    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.django_db
def test_get_me(user, auth_client):
    response = auth_client.get('/api/v1/accounts/users/me/')
    assert response.status_code == status.HTTP_200_OK
