import pytest
from django.contrib.auth import get_user_model
from rest_framework.test import APIClient


@pytest.fixture
def user():
    user_email = 'mark@email.com'
    user_pw = 'mark@email.com'
    user = get_user_model().objects.create_user(user_email, user_pw)
    user.is_verified = True
    user.save()
    return user

@pytest.fixture
def client():
    return APIClient()


@pytest.fixture
def auth_client(user, client):
    response = client.post('/api/v1/accounts/login/', dict(email='mark@email.com', password='mark@email.com'))
    token = response.data['token']
    client.credentials(HTTP_AUTHORIZATION='Token ' + token)
    return client
